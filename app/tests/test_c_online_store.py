#!/usr/bin/env python3

import unittest
from unittest.mock import MagicMock
from src.c_online_store import Product, Order, PaymentGateway

class TestOnlineStore(unittest.TestCase):
    def setUp(self):
        self.book = Product('Book', 10.0)
        self.pencil = Product('Pencil', 1.0)
        self.order = Order()
        self.order.add_product(self.book)
        self.order.add_product(self.pencil)

    def test_order_total(self):
        total = self.order.calculate_total()
        self.assertEqual(total, 11.0)

    def test_mocked_payment_gateway(self):
        PaymentGateway.charge = MagicMock(return_value=True)

        order_id = 123
        total = self.order.calculate_total()
        result = PaymentGateway.charge(order_id, total)

        self.assertTrue(result)
        PaymentGateway.charge.assert_called_once_with(order_id, total)

if __name__ == '__main__':
    unittest.main()
