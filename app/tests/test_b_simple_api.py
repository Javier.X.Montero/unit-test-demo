#!/usr/bin/env python3
import requests

import unittest
from unittest.mock import Mock, patch
from src.b_simple_api import WeatherAPI

class TestWeatherAPI(unittest.TestCase):
    @patch('requests.get')
    def test_get_current_weather_200(self, mock_get):
        mock_response = Mock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            'city': 'London',
            'temperature': 20,
            'humidity': 60,
            'pressure': 1013
        }
        mock_get.return_value = mock_response

        api = WeatherAPI()
        weather = api.get_current_weather('London', 'somekey')

        self.assertEqual(weather['city'], 'London')
        self.assertEqual(weather['temperature'], 20)
        self.assertEqual(weather['humidity'], 60)
        self.assertEqual(weather['pressure'], 1013)
        mock_get.assert_called_once_with('https://api.weatherapi.com/v1/current.json?key=somekey&q=London&aqi=no')

    @patch('requests.get')
    def test_get_current_weather_not_200(self, mock_get):
        mock_response = Mock()
        mock_response.status_code = 403
        mock_response.json.return_value = {
            'city': 'London',
            'temperature': 20,
            'humidity': 60,
            'pressure': 1013
        }
        mock_get.return_value = mock_response

        api = WeatherAPI()

        with self.assertRaises(Exception):
            api.get_current_weather('London', 'somekey')

        mock_get.assert_called_once_with('https://api.weatherapi.com/v1/current.json?key=somekey&q=London&aqi=no')

if __name__ == '__main__':
    unittest.main()