#!/usr/bin/env python3

import unittest
from unittest.mock import patch
from src.d_registration import register_user, UserExistsError, PasswordLengthError, PasswordMismatchError

class TestRegistration(unittest.TestCase):
    @patch('src.d_registration.check_username_exists')
    def test_register_user_user_exists(self, mock_check_username_exists):
        mock_check_username_exists.return_value = True
        with self.assertRaises(UserExistsError):
            register_user('testuser', 'testpassword', 'testpassword')

    def test_register_user_short_user(self):
        with self.assertRaises(PasswordLengthError):
            register_user('testuser', 'short', 'short')

    def test_register_user_password_mismatch(self):
        with self.assertRaises(PasswordMismatchError):
            register_user('testuser', 'testpassword', 'wrongpassword')

    def test_register_user_successful(self):
        self.assertEqual(register_user('testuser', 'testpassword', 'testpassword'), 'Registration successful')


if __name__ == '__main__':
    unittest.main()
