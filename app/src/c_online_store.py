#!/usr/bin/env python3

class Product:
    def __init__(self, name, price):
        self.name = name
        self.price = price

class Order:
    def __init__(self):
        self.items = []

    def add_product(self, product):
        self.items.append(product)

    def calculate_total(self):
        return sum(item.price for item in self.items)

class PaymentGateway:
    def charge(order_id, amount):
        # TODO: Rest of the owl, send the charge to the payment API
        pass