#!/usr/bin/env python3

class UserExistsError(Exception):
    pass

class PasswordLengthError(Exception):
    pass

class PasswordMismatchError(Exception):
    pass

def check_username_exists(username):
    # This function would interact with a database in a real-world scenario
    # For this example, it is left unimplemented
    pass

def register_user(username, password, confirm_password):
    if check_username_exists(username):
        raise UserExistsError('Username already exists')

    if len(password) < 8:
        raise PasswordLengthError('Password must be at least 8 characters long')

    if password != confirm_password:
        raise PasswordMismatchError('Passwords do not match')

    return 'Registration successful'
