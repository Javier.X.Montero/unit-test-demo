#!/usr/bin/env python3
import requests

class WeatherAPI:
    def __init__(self, base_url='https://api.weatherapi.com/v1'):
        self.base_url = base_url

    def get_current_weather(self, city, key):
        response = requests.get(f'{self.base_url}/current.json?key={key}&q={city}&aqi=no')
        if response.status_code != 200:
            raise Exception('API request failed')
        return response.json()