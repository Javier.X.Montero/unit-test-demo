# unit test demo
* Requirements:
  * python3
  * requests: ```pip3 install requests```
* Docs: [Unit tests docs](https://docs.python.org/3/library/unittest.html#module-unittest)
* Running: ```python -m unittest test/$name_of_test.py```